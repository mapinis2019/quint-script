#!/usr/bin/env bash

#Setup venv
virtualenv venv
source venv/bin/activate

# Install deps
pip3 install -e .

# Run the script
python main.py