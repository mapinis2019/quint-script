from distutils.core import setup

setup(
    name='Steam-stats',
    version='1.0',
    description='Steam Statistics',
    author='Mark Apinis',
    install_requires=[
        'steamodd',
        'jinja2'
    ]
)
