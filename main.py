import datetime
import json
import os
import smtplib

from jinja2 import Template

import steam
from steam.api import interface

#################################################################
#                                                               #
#            Project Name: QuintScript                          #
#            Developers: Mark Apinis, William Goodall           #
#            Date of Project Start: 5/31/16                     #
#            Date of Latest Update: ehh, check the git          #
#                                                               #
#            Made solely to honor the quote:                    #
#            "I have the whole summer to study"                 #
#                                      -Quint                   #
#                                                               #
#################################################################

"""
TODO:
timeSinceSchool()
calculatePercent()

PLAN:
main -> calculatePercent -> timeSinceSchool
                         -> jsonInter -> getTotalPlaytime
"""

API_KEY = os.environ.get("STEAM_API_KEY")
steam.api.key.set(API_KEY)

USERS_PATH = os.environ.get("DATA_FILE", None) or "data.json"
CONFIG_PATH = os.environ.get("USERS_FILE") or "users.json"

EMAIL_FROM = os.environ.get("EMAIL_FROM")
EMAIL_TO = os.environ.get("EMAIL_TO")
EMAIL_USERNAME = os.environ.get("EMAIL_USERNAME")
EMAIL_PASSWORD = os.environ.get("EMAIL_PASSWORD")

DUMMY = os.environ.get("DUMMY_EMAIL", None)

with open(CONFIG_PATH) as f:
    USERS = json.load(f)

with open(USERS_PATH) as f:
    DATA = json.load(f)


def getTotalPlaytime(steamid):
    """
    Interface with SteamODD to get total playtime in minutes
    :param steamid: SteamID 64 of the player
    :return: Total playtime in minutes
    """
    if DUMMY: return 2930475.394587


    playtime = 0
    games = interface('IPlayerService').GetOwnedGames(steamid=steamid, include_appinfo=0)
    for i in range(len(games['response']['games'])):
        playtime += games['response']['games'][i]['playtime_forever']
    return playtime

def makeIndividualReport(playtime, days, email, name):
    """
    Get a report for a player
    :param playtime: The player's playtime
    :param email: The player's email
    :param name: The player's name
    :return: None
    """

    # Load the message
    with open("report-template", 'r') as f:
        msgTemplate = Template(f.read())

    # Render the template
    msg = msgTemplate.render(
        sender=EMAIL_FROM,
        to=email,
        name=name,
        time=playtime,
        days=days
    )

    return msg

def makeGroupReport(players, email, name):
    """
    Make a group report containing all the players.
    """

    # Load the message
    with open("group-report-template", 'r') as f:
        msgTemplate = Template(f.read())

    # Render the template
    msg = msgTemplate.render(
        sender=EMAIL_FROM,
        to=email,
        name=name,
        players=players
    )

    return msg


def sendMessage(msg, to):
    """
    Send an email message to an address.
    """

    #Send to dummy if config'd
    to = DUMMY or to

    # Send mail
    server = smtplib.SMTP_SSL('smtp.gmail.com', 465)  # SSL port
    server.ehlo()
    server.login(EMAIL_USERNAME, EMAIL_PASSWORD)
    server.sendmail(EMAIL_FROM, to, msg)
    server.quit()

    #Log it
    print("Mail: {to:s}".format(to=to))

def gatherData():
    """
    Get data for all players defined in users.json
    """
    all = []

    for p in USERS["players"]:
        id = p["steamid"]

        playtime = getTotalPlaytime(id)
        prev = DATA.get(id, None)

        if prev:
            # Email playtime only if we have the player on record

            # Get the old timestamp
            oldTime = datetime.datetime.fromtimestamp(prev[0])
            nowTime = datetime.datetime.now()

            # Get the old playtime in minutes
            oldPlaytime = prev[1]
            playtimeDiff = playtime - oldPlaytime
            playtimeDiff /= 60

            # Get the difference in time
            timeDiff = (nowTime - oldTime).days

            # Log user's time
            print(
                "{name:s}: {time:.2f} hrs / {days:d} days"
                    .format(name=p['name'], time=playtimeDiff, days=timeDiff)
            )

            # Record player info in list of all players
            all.append({
                "name": p['name'],
                "playtime": playtimeDiff,
                "days": timeDiff,
                "email":p["email"] if 'email' in p else None
            })

        # Record playtime and timestamp
        DATA[id] = [datetime.datetime.now().timestamp(), playtime]

    return all

def main():
    """
    Ask user for SteamID, then send it to getTotalPlaytime, then call timeSinceSchool
    Ask for API Key, only until project is done and on server, which already has the key on it
    :return: None
    """

    print("Running script")

    data = gatherData()

    #Send individual reports.
    for p in data:
        if p['email']:
            sendMessage(makeIndividualReport(**p), p['email'])

    #Send group reports
    for r in USERS['group']:
        sendMessage(makeGroupReport(data, name=r[0], email=r[1]), to=r[1])

    # Save changes to data if actually run
    if not DUMMY:
        with open(USERS_PATH, 'w') as f:
            json.dump(DATA, f)


if __name__ == "__main__": main()
